#include <SADXModLoader.h>
#include "NowLoading.h"
#include "ScaleInfo.h";
#include <vector>
using namespace uiscale;

static NJS_TEXNAME loading_textures[3];
static NJS_TEXLIST loading_txlist{ arrayptrandlength(loading_textures) };
static bool NowLoadingLoaded = false;

// Hook to load the LOADING pvm
void LoadPVMHook()
{
	if (NowLoadingLoaded)
		return;
	texLoadTexturePvmFile("LOADING", &loading_txlist);
	gHelperFunctions->RegisterPermanentTexlist(&loading_txlist);
	NowLoadingLoaded = true;
}

// Draws "Now Loading" with SA1 logo instead of regular black fade
void Loading()
{
	Uint8 v0; // bl
	float right; // [esp+0h] [ebp-18h]
	float bottom; // [esp+4h] [ebp-14h]
	float r; // [esp+0h] [ebp-38h]
	NJS_SPRITE _sp; // [esp+18h] [ebp-20h] BYREF

	v0 = usFadeLevel;
	if (usFadeLevel >= 255u)
	{
		v0 = 255;
	}
	njSetZCompare(7u);
	njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
	njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_INVSRCALPHA);

	// Draw black background
	bottom = ScreenRaitoY * 480.0;
	right = ScreenRaitoX * 640.0;
	late_DrawBoxFill2D(0.0, 0.0, right, bottom, 22045.0, v0 << 24, LATE_LIG);

	// Draw "Now loading" sprites
	gHelperFunctions->PushScaleUI(Align::Align_Center, false, 1.0f, 1.0f);
	njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
	njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_ONE);
	r = v0 * 0.0039215689f;
	SetMaterial(r, 1.0f, 1.0f, 1.0f);
	_sp.tlist = &loading_txlist;
	_sp.tanim = &anim_fade_0;
	_sp.sx = 1.0f;
	_sp.sy = 1.0f;
	_sp.p.x = 320.0f;
	_sp.p.y = 240.0f;
	for (int i = 0; i < 2; i++)
	{
		late_DrawSprite2D(&_sp, i, 22047.0, NJD_SPRITE_ALPHA | NJD_SPRITE_COLOR, LATE_LIG);
	}
	ResetMaterial();
	gHelperFunctions->PopScaleUI();
	njSetZCompare(1u);
}

// Force draw "Now Loading" with maximum opacity
void LoadingMissingFix(float left, float top, float right, float bottom, float depth, int color)
{
	NJS_SPRITE _sp; // [esp+18h] [ebp-20h] BYREF

	njSetZCompare(7u);
	ds_DrawBoxFill2D(0.0, 0.0, right, bottom, -1.0, 0xFF000000);
	gHelperFunctions->PushScaleUI(Align::Align_Center, false, 1.0f, 1.0f);
	njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
	njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_ONE);
	SetMaterial(1.0f, 1.0f, 1.0f, 1.0f);
	_sp.tlist = &loading_txlist;
	_sp.tanim = &anim_fade_0;
	_sp.sx = 1.0f;
	_sp.sy = 1.0f;
	_sp.p.x = 320.0f;
	_sp.p.y = 240.0f;
	njSetZCompare(3u);
	for (int i = 0; i < 2; i++)
	{
		dsDrawSprite2D(&_sp, i, -1.0f, NJD_SPRITE_ALPHA | NJD_SPRITE_COLOR);
	}
	ResetMaterial();
	gHelperFunctions->PopScaleUI();
	npSetZCompare();
}

// Replace the event fade with "Now Loading"
void EVFade_r(task* a1)
{
	taskwk* twp = a1->twp;
	usFadeLevel = (__int16)twp->value.f;
	Loading();
}

// Replace the empty background with "Now Loading" when screen fade is supposed to be drawn
void BackgroundHack(NJS_POINT2COL* p, Int n, Float pri, Uint32 attr)
{
	if (usMainFadeStatus != 0)
		LoadingMissingFix(0, 0, 0, 0, 0, 0);
	else
		njDrawPolygon2D(p, n, pri, attr);
}

// Replace the level title card
Sint32 __cdecl StageNameMode_r()
{
	_BOOL1 v0; // cc
	Sint32 result; // eax
	float v2; // [esp+0h] [ebp-Ch]
	float a2; // [esp+0h] [ebp-Ch]
	float a2a; // [esp+0h] [ebp-Ch]
	float v5; // [esp+4h] [ebp-8h]
	float a3; // [esp+4h] [ebp-8h]
	float a3a; // [esp+4h] [ebp-8h]

	v0 = StageNameWork.timer++ <= StageNameWork.DispTime;
	if (!v0 && !now_saving)
	{
		late_ReleaseTexture(StageNameWork.texlist);
		result = 1;
		pause_flg = 1;
		return result;
	}
	LoadingMissingFix(0, 0, 0, 0, 0, 0);
	return 1;
}

void NowLoading_Init()
{
	WriteCall((void*)0x00420992, LoadPVMHook);
	WriteJump((void*)0x0047E170, StageNameMode_r);
	WriteCall((void*)0x0078B922, BackgroundHack);
	WriteJump((void*)0x00412F70, EVFade_r);
	WriteCall((void*)0x0040901C, LoadingMissingFix);
	WriteCall((void*)0x0040901C, LoadingMissingFix);
	WriteJump(ScreenFade_Draw, Loading);
}